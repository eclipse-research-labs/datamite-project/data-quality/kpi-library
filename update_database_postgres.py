# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

import psycopg2
import inspect
import importlib
# needed attributes
cur = None
conn = None
try:
    # connect to your postgres DB
    conn = psycopg2.connect(database="eda",
                            user="postgres",
                            password="postgres",
                            host="localhost",
                            port=54325)
    # open a cursor to perform database operations
    cur = conn.cursor()
    # drop tables
    cur.execute("TRUNCATE TABLE parameter_in_metric")
    cur.execute("TRUNCATE TABLE parameter CASCADE")
    cur.execute("TRUNCATE TABLE metric CASCADE")
    # add new information in the tables
    #   1. import module
    module = importlib.import_module('kpi_library')
    #   2. obtain all metrics
    metric_model = getattr(module, 'MetricModel')
    metrics = [metric[1] for metric in inspect.getmembers(module, inspect.isclass) if metric[0] != 'MetricModel']
    #   3. get information of the metrics and store them
    for metric in metrics:
        if issubclass(metric, metric_model):
            met = metric()
            # save metric (the metric saves the parameters into the parameter table (if it was not created before and
            # gets its identifier to add it into the middle table with the identifier of this metric)
            cur.execute(
                "INSERT INTO metric (identifier, keyword, title, definition, expected_data_type, dimension, category) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s)", (met.identifier, met.keyword, met.title, met.definition,
                                                        met.expected_data_type, met.dimension, met.category))
            if met.has_parameters is not None:
                for param in met.has_parameters:
                    cur.execute(f"SELECT id FROM parameter WHERE name='{param.name}'")
                    p = cur.fetchone()
                    if p is None:
                        cur.execute(
                            "INSERT INTO parameter (name, data_type, description, possible_values, default_value) "
                            "VALUES (%s, %s, %s, %s, %s)", (param.name, param.data_type, param.description,
                                                            param.possible_values, param.default_value))
                        cur.execute(f"SELECT id FROM parameter WHERE name='{param.name}'")
                        p = cur.fetchone()
                    cur.execute("INSERT INTO parameter_in_metric (metric_key, parameter_id) VALUES (%s, %s)",
                                (met.identifier, int(p[0])))
    # commit changes
    conn.commit()
except Exception as e:
    print(f"Error: {e}.")
finally:
    # close cursor and connection
    if cur is not None:
        cur.close()
    if conn is not None:
        conn.close()
