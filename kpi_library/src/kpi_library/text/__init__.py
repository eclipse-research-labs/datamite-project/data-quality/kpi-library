# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

# kpi_library/text/__init__.py
from .abbreviations import TextAbbreviations
from .abbreviations_frequency import TextAbbreviationsFrequency
from .abbreviations_frequency_percent import TextAbbreviationsFrequencyPercent
from .acronyms import TextAcronyms
from .acronyms_frequency import TextAcronymsFrequency
from .acronyms_frequency_percent import TextAcronymsFrequencyPercent
from .collocations_distribution import TextCollocationDistribution
from .collocations_distribution_percent import TextCollocationDistributionPercent
from .count_characters import TextCountCharacters
from .count_characters_percent import TextCountCharactersPercent
from .count_sentences import TextCountSentences
from .count_sentences_percent import TextCountSentencesPercent
from .count_words import TextCountWords
from .count_words_percent import TextCountWordsPercent
from .distribution_less_frequent_elements import TextDistributionLessFrequentElement
from .distribution_less_frequent_elements_percent import TextDistributionLessFrequentElementPercent
from .distribution_most_frequent_elements import TextDistributionMostFrequentElement
from .distribution_most_frequent_elements_percent import TextDistributionMostFrequentElementPercent
from .lexical_diversity_distinct import TextLexicalDiversityDistinct
from .lexical_diversity_total import TextLexicalDiversityTotal
from .lexical_diversity_uniqueness import TextLexicalDiversityUniqueness
from .longest_frequent_words import TextLongestFrequentWords
from .longest_words import TextLongestWords
from .spell_mistakes import TextSpellMistakes
from .spell_mistakes_frequency import TextSpellMistakesFrequency
from .spell_mistakes_frequency_percent import TextSpellMistakesFrequencyPercent
from .uppercase import TextUppercase
from .uppercase_frequency import TextUppercaseFrequency
from .uppercase_frequency_percent import TextUppercaseFrequencyPercent
