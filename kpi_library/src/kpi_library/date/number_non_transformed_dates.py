# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

# kpi_library/date/number_non_transformed_dates.py
import json
import pandas as pd

from typing import Optional
from ..model import MetricModel, ParameterModel
from ..errors import DataTypeError, EmptyDatasetError
from ..result_types import ResultTypes


class DateNumberNonTransformedDates(MetricModel):
    """
    This metric computes the number of values that could not be transformed into the given or inferred date format.

    Example
    -------
    >>> srs = pd.Series(["2022-20-03 00:00:00", "2022-21-03 00:00:00", "2022-22-03 00:00:00", "2022-23-03 00:00:00", \
    "2022-24-03 00:00:00", "2022-25-03 00:00:00"], name='timestamp')
    >>> c = DateNumberNonTransformedDates()
    >>> c.run(srs, date_format=r'%Y-%d-%m %H:%M:%S')
    0
    >>> c.run(srs)
    6
    >>> srs = pd.Series(["2022-20-03", "2022-03-21", "2022-22-03", "2022-23-03", "2022-24-03", "2022-25-03"])
    >>> c.run(srs, date_format=r'%Y-%d-%m')
    1
    >>> c.to_dqv(srs, date_format=r'%Y-%d-%m')
    [{'dqv_isMeasurementOf': 'date.number_non_transformed_dates', 'dqv_computedOn': '', 'rdf_datatype': 'Integer', \
'ddqv_hasParameters': [{'parameter_name': 'date_format', 'value': '"%Y-%d-%m"'}], 'dqv_value': '1'}]
    >>> srs = pd.Series([None, None, None], name='timestamp')
    >>> c.run(srs)
    >>> c.to_dqv(srs)
    [{'dqv_isMeasurementOf': 'date.number_non_transformed_dates', 'dqv_computedOn': 'timestamp', 'rdf_datatype': 'In\
teger', 'ddqv_hasParameters': [{'parameter_name': 'date_format', 'value': 'null'}], 'dqv_value': 'null'}]
    >>> c.to_dqv(srs, date_format='%Y-%d-%m')
    [{'dqv_isMeasurementOf': 'date.number_non_transformed_dates', 'dqv_computedOn': 'timestamp', 'rdf_datatype': 'In\
teger', 'ddqv_hasParameters': [{'parameter_name': 'date_format', 'value': '"%Y-%d-%m"'}], 'dqv_value': 'null'}]
    """
    def __init__(self):
        super(DateNumberNonTransformedDates, self).__init__(
            identifier='date.number_non_transformed_dates',
            keyword='DateNumberNonTransformedDates',
            title='Number of non-transformed dates',
            definition='Number of non-transformed dates.',
            expected_data_type=str(ResultTypes.INT.value),
            dimension='profile',
            category='inherent'
        )
        self.has_parameters = [
            ParameterModel(name='date_format', data_type=str(ResultTypes.STRING.value), possible_values=None,
                           default_value=None, description='The format to parse the dates.')]

    def to_dqv(self, data: pd.Series, **kwargs):
        params = {'date_format': kwargs.get('date_format', None)}
        # run method
        try:
            result = self.run(data, **kwargs)
        except (EmptyDatasetError, DataTypeError):
            # error found
            return [{
                'dqv_isMeasurementOf': f'{self.identifier}',
                'dqv_computedOn': "",
                'rdf_datatype': "Error",
                'ddqv_hasParameters': self._turn_dictionary_to_parameter(parameters=params),
                'dqv_value': json.dumps(None)
            }]
        # no error, result obtained
        return [{
            'dqv_isMeasurementOf': f'{self.identifier}',
            'dqv_computedOn': "" if data.name is None else data.name,
            'rdf_datatype': self.expected_data_type,
            'ddqv_hasParameters': self._turn_dictionary_to_parameter(parameters=params),
            'dqv_value': json.dumps(result)
        }]

    def run(self, data: pd.Series, **kwargs) -> Optional[float]:
        """
        This method returns the number of entries that could not be transformed into dates.

        Parameters
        ----------
        data: :obj:`pandas.Series`
            Object containing the data to be processed.

        Returns
        -------
        _: int, optional.
            Number of non-transformed dates.
        """
        # check data
        date_format = kwargs.get('date_format', None)
        srs = self._check_date_data(data, date_format)
        # check if dataset is empty
        if srs.empty:
            return None
        # compute statistic
        return int(srs.isna().sum())
