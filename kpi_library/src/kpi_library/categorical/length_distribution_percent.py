# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

# kpi_library/categorical/length_distribution_percent.py
import json
import numpy as np
import pandas as pd

from typing import List, Dict
from ..model import MetricModel
from ..errors import DataTypeError, EmptyDatasetError
from ..result_types import ResultTypes


class CategoricalLengthDistributionPercent(MetricModel):
    """
    This metric gets the percentage of occurrence of the length in characters of the entries in the categorical data
    given as parameter.

    Example
    -------
    >>> c = CategoricalLengthDistributionPercent()
    >>> srs = pd.Series(['a', 'b', 'c', 'a', 'a', 'b'], name='ID')
    >>> c.run(srs)
    [{'item': 1, 'frequency': 100.0}]
    >>> c.to_dqv(srs)
    [{'dqv_isMeasurementOf': 'categorical.length_distribution_percent', 'dqv_computedOn': 'ID', 'rdf_datatype': 'List<\
Map<String,String>>', 'ddqv_hasParameters': [], 'dqv_value': '[{"item": 1, "frequency": 100.0}]'}]
    >>> srs = pd.Series(['a', None, None, 'a', 'a', 'b'], name='ID')
    >>> c.run(srs)
    [{'item': 1, 'frequency': 66.67}]
    >>> c.to_dqv(srs)
    [{'dqv_isMeasurementOf': 'categorical.length_distribution_percent', 'dqv_computedOn': 'ID', 'rdf_datatype': 'List<\
Map<String,String>>', 'ddqv_hasParameters': [], 'dqv_value': '[{"item": 1, "frequency": 66.67}]'}]
    >>> c.to_dqv(pd.Series())
    [{'dqv_isMeasurementOf': 'categorical.length_distribution_percent', 'dqv_computedOn': '', 'rdf_datatype': 'Error',\
 'ddqv_hasParameters': [], 'dqv_value': 'null'}]
    >>> c.to_dqv(pd.Series([123.12313, 1231.23421, 1234124.2134]))
    [{'dqv_isMeasurementOf': 'categorical.length_distribution_percent', 'dqv_computedOn': '', 'rdf_datatype': 'Error',\
 'ddqv_hasParameters': [], 'dqv_value': 'null'}]
    >>> c.to_dqv(pd.Series(["2022-03-23", "2022-03-24", "2022-03-25", "2022-03-26", "2022-03-27", "2022-03-28"]))
    [{'dqv_isMeasurementOf': 'categorical.length_distribution_percent', 'dqv_computedOn': '', 'rdf_datatype': 'Error',\
 'ddqv_hasParameters': [], 'dqv_value': 'null'}]
    >>> c.run(pd.Series([None, None, None]))
    []
    >>> c.to_dqv(pd.Series([None, None, None]))
    [{'dqv_isMeasurementOf': 'categorical.length_distribution_percent', 'dqv_computedOn': '', 'rdf_datatype': 'List<M\
ap<String,String>>', 'ddqv_hasParameters': [], 'dqv_value': '[]'}]
    """
    def __init__(self):
        super(CategoricalLengthDistributionPercent, self).__init__(
            identifier='categorical.length_distribution_percent',
            keyword='CategoricalLengthDistributionPercent',
            title='Length distribution in percentage',
            definition='Length distribution in characters of the categorical data as percentage of occurrence.',
            expected_data_type=str(ResultTypes.DISTRIBUTION_INT.value),
            dimension='profile',
            category='inherent'
        )

    def to_dqv(self, data: pd.Series, **kwargs):
        # run method
        try:
            result = self.run(data, **kwargs)
        except (EmptyDatasetError, DataTypeError):
            # error found
            return [{
                'dqv_isMeasurementOf': f'{self.identifier}',
                'dqv_computedOn': "",
                'rdf_datatype': "Error",
                'ddqv_hasParameters': [],
                'dqv_value': json.dumps(None)
            }]
        # no error, result obtained
        return [{
            'dqv_isMeasurementOf': f'{self.identifier}',
            'dqv_computedOn': "" if data.name is None else data.name,
            'rdf_datatype': self.expected_data_type,
            'ddqv_hasParameters': [],
            'dqv_value': json.dumps(result)
        }]

    def run(self, data: pd.Series, **kwargs) -> List[Dict[str, int]]:
        """
        This method gets the percentage of occurrence of the length in characters of the entries in the categorical data
        given as parameter.

        Parameters
        ----------
        data: :obj:`pandas.Series`
            Object containing the data to be processed.

        Returns
        -------
        _: :obj:`list` of :obj:`dict`
            Length distribution in percentage.
        """
        srs = self._check_categorical_data(data)
        # check if dataset is empty
        if srs.empty:
            return []
        # prepare data
        srs = srs.to_numpy()
        v_len = np.vectorize(len)
        # compute length of each entry and frequency of each length
        data_lengths = v_len(srs)
        uniques, counts = np.unique(data_lengths, return_counts=True)
        return [{
            "item": int(item), "frequency": float(round((frequency/data.shape[0])*100, 2))
        } for item, frequency in zip(uniques, counts)]
