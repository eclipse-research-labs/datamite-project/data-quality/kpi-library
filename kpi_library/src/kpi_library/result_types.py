# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

# kpi_library/result_types.py
from enum import Enum


class ResultTypes(Enum):
    # constants
    __LIST_MAP_STRING = 'List<Map<String,String>>'
    __LIST_MAP_SERIALIZABLE = 'List<Map<String,Serializable>>'

    # enum classes
    INT = 'Integer'
    FLOAT = 'Float'
    STRING = 'String'
    DATE = 'DateTime'
    TIMEDELTA = 'Timedelta'
    BOOL = 'Boolean'

    DISTRIBUTION_INT = __LIST_MAP_STRING
    DISTRIBUTION_FLOAT = __LIST_MAP_STRING

    BOX_PLOT = 'Map<String,String>'
    HISTOGRAM = __LIST_MAP_STRING

    LIST_STR = 'List<String>'

    CAT_DISTRIBUTION_INT = __LIST_MAP_SERIALIZABLE
    CAT_DISTRIBUTION_FLOAT = __LIST_MAP_SERIALIZABLE

    INFER_FREQUENCY = 'Map<String,Integer>'
    PLOT = __LIST_MAP_STRING

    CAT_BOX_PLOT = __LIST_MAP_SERIALIZABLE

    CROSS_TABULATION = __LIST_MAP_STRING
