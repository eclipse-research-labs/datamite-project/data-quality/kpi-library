# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

# kpi_library/numeric/__init__.py
from .box_plot import NumericBoxPlot
from .coefficient_variation import NumericCoefficientVariation
from .count_negatives import NumericCountNegatives
from .count_negatives_percent import NumericCountNegativesPercent
from .count_zeros import NumericCountZeros
from .count_zeros_percent import NumericCountZerosPercent
from .fifth_percentile import NumericFifthPercentile
from .first_quartile import NumericFirstQuartile
from .histogram import NumericHistogram
from .iqr import NumericIqr
from .kurtosis import NumericKurtosis
from .mad import NumericMad
from .max import NumericMaximum
from .mean import NumericMean
from .median import NumericMedian
from .min import NumericMinimum
from .mode import NumericMode
from .mode_frequency import NumericModeFrequency
from .mode_frequency_percent import NumericModeFrequencyPercent
from .ninety_fifth_percentile import NumericNinetyFifthPercentile
from .outliers import NumericOutliers
from .outliers_percent import NumericOutliersPercent
from .range import NumericRange
from .skewness import NumericSkewness
from .std import NumericStd
from .sum import NumericSum
from .third_quartile import NumericThirdQuartile
