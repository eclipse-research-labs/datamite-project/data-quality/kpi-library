# Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     [name] - [contribution]

from pymongo import MongoClient
import inspect
import importlib
from typing import List
# connecting to the MongoDB server
client = MongoClient('localhost', 27017)
print('Connection open.')
# create or access to the database called 'kpi_library'
db = client['kpi_library']
# create or access to the metrics and parameters collection
metric_collection = db['metrics']
# drop all documents in the metrics collection
metric_collection.delete_many({})
print('The database has been cleaned')
# insert metrics
#   1. import module
module = importlib.import_module('kpi_library')
#   2. obtain all metrics
metric_model = getattr(module, 'MetricModel')
metrics: List[metric_model] = [
    metric[1] for metric in inspect.getmembers(module, inspect.isclass) if metric[0] != 'MetricModel']
print('Getting the metric models and inserting the metrics in the database')
#   3. get information of the metrics and store them
for metric in metrics:
    if issubclass(metric, metric_model):
        metric_collection.insert_one(metric().get())
# close the connection with the mongodb
print('Closing the database connection')
client.close()
