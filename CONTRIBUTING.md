<!--
  ~ Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

## CONTRIBUTION GUIDELINES

This repository follow the same rules that you cand find on the [main contribution file](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/docs/-/blob/main/CONTRIBUTING.md) of DATAMITE project.

Please, **read it carefully before making your contribution**, especially if the purpose of your contribution is to update or provide new code on this repository.

If you have any question, comment or problem, do not hesitate to contact us at:

* Jordi Arjona: jarjona@iti.es (repository's owner)
* Arántzazu López: alopezlarrainzar@iti.es