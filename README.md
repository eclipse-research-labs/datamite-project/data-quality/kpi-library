<!--
  ~ Copyright (c) 2024 Instituto Tecnologico de Informatica (ITI)
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

# DESCRIPTION
This project will host the developments for the KPI library, one of the components of the Data Quality Module, as can be seen in the architecture figure below.

![architecture_KPI](docs/figures/arch_extended_KPI.png)

The kpi library is a Python library designed to implement different profiling techniques for structured data. This library seeks to analyze data in a inherit way, that is, being able of processing any type of data that can be found in a structured dataset. The methods implemented can process the entire dataset generally, without going into the data type of each column; each column, depending on the data it contains (numbers, categories, dates or texts); or the relationship between two different columns.

In a few words, the implemented methods or metrics in this library are grouped according to the following categories:

- General: they can process the entire dataset and they do it without entering in what type of data they are analyzing.

- Numeric: they process a numeric column at a time.

- Categorical: they process a column of categorical data at a time, i.e., they analyze data that are composed of categories such as a genre or a country column.

- Date: they process a time column at a time.

- Text: they process a text column at a time.

- Bi-variate analysis: they have implemented a correlation method (it studies the relationship between two numeric columns) and a conditional frequency distribution method (it studies the relationship between a categorical and text column).

- Time series: they analyze a time series, i.e., they find the relationship between a date and a numeric column.

- Techniques of visualization of two column of data: they visualize the relationship between two columns with different techniques and for different types of data.

# Table of contents

1. Relationship with DQV
2. Implemented metrics
    1. General
    2. Numeric
    3. Categorical
    4. Date
    5. Text
    6. Bi-analysis
    7. Time series
    8. Visualization methods
3. Installation
4. Usage

# Relationship with DQV

This entire library was intended to match the DQV metadata standard, since the results after processing a dataset with any of the implemented metrics are metadata. In the case of this library, there are two DQV objects of special interest: _Metric_ and _QualityMeasurement_. _Metric_ is the class that keeps all the information of each quality method and _QualityMeasurement_ keeps the result after processing data.

For this purpose, a class called _MetricModel_ has been develop, from which all the implemented metric in the library inherit and which is characterized for:

- Having the following attributes:
    - identifier
    - keyword: name of the metric which will help us in the implementation of each metric.
    - title
    - definition
    - expected_data_type: type of the result the metric returns.
    - dimension: dimension in which this metric belongs.
    - category: category in which the dimension of the metric belongs.
    - has_parameters: the parameters this metric will need.

- Methods:
    - run: it process the data to obtain the metadata.
    - to_dqv: it process the data but rather than returning the result in a plane format, it returns it following the structure of the DQV object _QualityMeasurement_. The next code shows the object this method returns:
    ```
    {
        'dqv_isMeasurementOf': 'metric identifier',
        'dqv_computedOn': 'name of the data the metric has processed',
        'rdf_datatype': 'type of the result the metric returns',
        'ddqv_hasParameters': [{
            'parameter_name': 'parameter name',
            'value': 'value of the parameter'
        }],
        'dqv_value': 'result obtained after processing the dataset'
    }
    ```

# Implemented metrics

## General

- `position` (GeneralPosition) - returns the position of each column in the data.

- `count` (GeneralCount) - counts the number of non-null values per column and returns a list of dictionaries, one per column that differ from each other thanks to the `data_name` key.

- `num_rows` (GeneralNumRows) - returns the number of rows in the data.

- `num_columns` (GeneralNumColumns) - returns the number of columns in the data.

- `duplicated_entries` (GeneralDuplicatedEntries) - computes the number of rows that are duplicated in the dataset.

- `duplicated_entries_percent` (GeneralDuplicatedEntriesPercent) - computes the percentage of rows that are duplicated in the dataset.

- `completeness` (GeneralCompleteness) - computes the number of missing values, i.e., those fields that contain a null value, per column.

- `completeness_percent` (GeneralCompletenessPercent) - computes the percentage of missing values per column in the dataset.

- `unique_entries` (GeneralUniqueEntries) - computes the number of different elements in each column.

- `unique_entries_percent` (GeneralUniqueEntriesPercent) - computes the percentage of different elements in each column.

- `data_types` (GeneralDataTypes) - infers the data type of each column.

- `memory_usage_bytes` (GeneralMemoryUsageBytes) - computes the memory needed for each column in bytes.

## Numeric

- `fifth_percentile` (NumericFifthPercentile) - computes the 5th-percentile.

- `first_quartile` (NumericFirstQuartile) - computes the first quartile (25th-percentile).

- `median` (NumericMedian) - computes the median (second quartile) of the data.

- `third_quartile` (NumericThirdQuartile) - computes the third quartile (75th-percentile).

- `ninety_fifth_percentile` (NumericNinetyFifthPercentile) - computes the 95th-percentile.

- `mean` (NumericMean) - computes the mean value.

- `maximum` (NumericMaximum) - returns the maximum value.

- `minimum` (NumericMinimum) - returns the minimum value of the data.

- `range` (NumericRange) - computes the difference between the maximum and minimum value of the data.

- `iqr` (interquartile range) (NumericIqr) - computes the different between the third and first quartile of the data.

- `kurtosis` (NumericKurtosis) - computes the kurtosis value of the data.

- `skewness` (NumericSkewness) - computes the skewness value.

- `coefficient_variation` (NumericCoefficientVariation) - computes the coefficient of variation, i.e., the ratio of the standard deviation to the mean, showing the extent of variability in relation to the mean of the population.

- `mad` (NumericMad) - computes the Mean Absolute Deviation (MAD), i.e., the average distance between each data point and the mean, giving an idea about the variability in a dataset.

- `count_zeros` (NumericCountZeros) - counts the number of entries that are equal to zero.

- `count_zeros_percent` (NumericCountZerosPercent) - returns the percentage of entries that are equal to zero.

- `count_negative` (NumericCountNegative) - counts the number of negative values in the data.

- `count_negative_percent` (NumericCountNegativePercent) - returns the percentage of entries that are below zero.

- `mode` (NumericMode) - returns the most frequent element of the data.

- `mode_frequency` (NumericModeFrequency) - returns the frequency of the most frequent element in the data.

- `mode_frequency_percent` (NumericModeFrequencyPercent) - returns the frequency as percentage of the most frequent element in the data.

- `outliers` (NumericOutliers) - returns the number of possible outliers by using the interquartile range method.

- `outliers_percent` (NumericOutliersPercent) - returns the percentage of possible outliers.

- `std` (NumericStd) - computes the standard deviation of the data.

- `sum` (NumericSum) - sums all values in the data.

- `box_plot` (NumericBoxPlot) - calculates the statistical information needed to build a box-and-whiskers plot, i.e., the minimum and maximum value, the first, second (median) and third quartile, the mean and the list of outliers (data that fall outside the interval described by the whiskers). This information is saved in a dictionary format with their respective keys.

- `histogram` (NumericHistogram) - this metric divides the entire range of values into a series of intervals (bins) and then counts how many values fall into each interval. Thus, it returns a list of dictionaries containing the values of the interval (limits) and the number of elements falling in the specified bin (frequency).

## Categorical

- `mode` (CategoricalMode) - returns the most frequent element of the data.

- `mode_frequency` (CategoricalModeFrequency) - returns the frequency of the most frequent element in the data.

- `mode_frequency_percent` (CategoricalModeFrequencyPercent) - returns the frequency as percentage of the most frequent element in the data.

- `length_distribution` (CategoricalLengthDistribution) - computes the character length distribution of the data and returns a list of dictionaries containing the length in characters (item) and the frequency at which it appears (frequency).

- `length_distribution_percent` (CategoricalLengthDistributionPercent) - computes the character length distribution of the data and returns a list of dictionaries containing the length in characters (item) and the frequency as percentage at which it appears (frequency).

- `outliers` (CategoricalOutliers) - has the same functionality as the outlier metric of the numerical module.

- `outliers_percent` (CategoricalOutliersPercent) - has the same functionality as the outlier_percent metric of the numerical module.

- `frequency_distribution` (CategoricalFrequencyDistribution) - computes the frequency of occurrence of each element and returns an ordered list of dictionaries containing the element (item) and its frequency (frequency). The list is sorted according to the frequency of each element, so that the first dictionary will contain the most frequent element.

- `frequency_distribution_percent` (CategoricalFrequencyDistributionPercent) - computes the percentage of occurrence of each element and returns an ordered list of dictionaries according to the frequency of each element.

## Date

- `number_non_transformed_dates` (DateNumberNonTransformedDates) - gets the number of values that do not follow the date format that has been inferred or passed as input.

- `fifth_percentile` (DateFifthPercentile)

- `first_quartile` (DateFirstQuartile)

- `median` (DateMedian)

- `third_quartile` (DateThirdQuartile)

- `ninety_fifth_percentile` (DateNinetyFifthPercentile)

- `minimum` (DateMinimum)

- `maximum` (DateMaximum)

- `mean` (DateMean)

- `range` (Daterange)

- `iqr` (DateIqr)

- `std` (DateStd)

- `mode` (DateMode)

- `mode_frequency` (DateModeFrequency)

- `mode_frequency_percent` (DateModeFrequencyPercent)

- `frequency_distribution` (DateFrequencyDistribution) - computes the number of times each date appears in the data.

- `frequency_distribution_percent` (DateFrequencyDistributionPercent) - computes the percentage in which each date appears in the data.

- `histogram` (DateHistogram) - studies how the data is distributed, dividing the date range into a specific number of intervals (bins) and calculating the number of dates that fall within each bin. This metric works very similarly to the histogram metric of the numerical module.

- `histogram_percent` (DateHistogramPercent) - provides the same information as the histogram metric but instead of returning the frequency as an integer, i.e. the number of dates that fall within each bin, it returns the percentage of the data that fall within each bin.

Nothing has been commented on some metrics, because they have a similar operation to the metrics, with the same name, of the modules that have been previously explained.

## Text

- `count_sentences` (TextCountSentences) - counts the number of sentences and returns a list of dictionaries containing the sentences length (item) and its frequency (frequency).

- `count_sentences_percent` (TextCountSentencesPercent) - computes the sentence-length of each text and returns the percentage each frequency appears in the data.

- `count_words` (TextCountWords) - this metric is similar to the `count_sentences` metric but it counts the number of words in each entry.

- `count_words_percent` (TextCountWordsPercent) - similar to the metric `count_sentences_percent` but counting the number of words.

- `count_characters` (TextCountCharacters) - it is similar to the `count_sentences` metric but it counts the number of characters in each entry.

- `count_characters_percent` (TextCountCharactersPercent) - similar to the metric `count_sentences_percent` but counting the number of characters.

- `longest_words` (TextLongestWords) - tokenizes the texts into words and returns a list of those with a length greater than the length given as input (`length` parameter).

- `longest_frequent_words` (TextLongestFrequentWords) - also tokenizes the texts into words and returns a list of those with a length greater than the length given as parameter (`length`) and with a frequency of occurrence greater than that given as input (`frequency`).

- `collocations_distribution` (TextCollocationsDistribution) - computes the frequency distribution of the collocations in the data, being a collocation a combination of words that appear more frequently than usual, and returns the most frequently occurring collocations, i.e., an ordered list of dictionaries containing the collocation (`item`), and the frequency of occurrence (`frequency`). Depending on the parameter collocation this metric will return the collocations of two words (`bigram`), three words (`trigram`) or both (`both`).

- `collocations_distribution_percent` (TextCollocationsDistributionPercent) - is similar to the metric `collocations_distribution` but the frequency is the percentage the collocation appears in the total list of collocations obtained.

- `distribution_most_frequent_elements` (TextDistributionMostFrequentElements) - calculates the frequency distribution of elements in the data and returns an ordered list of dictionaries containing the element (`item`) and the frequency of occurrence (`frequency`). This method returns only the number of elements specified as parameter (`num_items`) and, depending on the parameters, it will calculate the frequency of the entries or the words that form the texts (`tokenization`), and will eliminate or not those words that can be interpreted as stopwords (`stopwords`).

- `distribution_most_frequent_elements_percent` (TextDistributionMostFrequentElementsPercent) - calculates the distribution of the percentage of occurrence of the elements found in the data and returns an ordered list of dictionaries containing the element (`item`) and the percentage (`frequency`). This method returns only the number of elements specified as parameter (`num_items`) and, depending on the parameters, it will calculate the frequency of the entries or the words that form the texts (`tokenization`), and will eliminate or not those words that can be interpreted as stopwords (`stopwords`).

- `distribution_less_frequent_elements` (TextDistributionLessFrequentElements) - seeks, on the other hand, to visualize those elements that appear less frequently, so it will return the same list but sorted in reverse order, with the least frequent elements in first place.

- `distribution_less_frequent_elements_percent` (TextDistributionLessFrequentElementsPercent) - returns a list of dictionaries containing the least frequent elements in the data with the `frequency` field as the percentage of occurrence of each element.

- `abbreviations` (TextAbbreviations) - an abbreviation is a shortened form of a word or phrase, for example the abbreviation of kilogram is Kg. This method returns the a list with all the abbreviations found in the texts.

- `abbreviations_frequency` (TextAbbreviationsFrequency) - returns the total number of abbreviations found in the texts.

- `abbreviations_frequency_percent` (TextAbbreviationsFrequencyPercent) - returns the percentage of abbreviations in the texts in comparison with the total number of words.

- `acronyms` (TextAcronyms) - an acronym is an abbreviation formed from the initial letters of other words and pronounced as a word (e.g. AI stands for Artificial Intelligence). This metric returns a list containing the acronyms found in the data.

- `acronyms_frequency` (TextAcronymsFrequency) - returns the total number of acronyms in the texts.

- `acronyms_frequency_percent` (TextAcronymsFrequencyPercent) - is similar to `acronym_frequency` but it returns the percentage of acronyms in the texts.

- `uppercase` (TextUppercase) - uppercase words refer to text or letters that are written in capital letters. This metric returns a list containing the uppercase words found in the texts.

- `uppercase_frequency` (TextUppercaseFrequency) - returns the total number of uppercase words in the texts.

- `uppercase_frequency_percent` (TextUppercaseFrequencyPercent) - returns the percentage of uppercase words in the texts.

- `spell_mistakes` (TextSpellMistakes) - spell mistakes refers to errors in spelling, i.e., words that are written incorrectly according to the accepted conventions of a particular language. This metric returns a list containing the spell mistakes found in the texts.

- `spell_mistakes_frequency` (TextSpellMistakesFrequency) - returns the total number of spell mistakes in the texts.

- `spell_mistakes_frequency_percent` (TextSpellMistakesFrequencyPercent) - returns the percentage of spell mistakes in the texts.

- `lexical_diversity_total` (TextLexicalDiversityTotal) - returns the total number of words in the texts.

- `lexical_diversity_distinct` (TextLexicalDiversityDistinct) - returns the number of distinct words in the texts.

- `lexical_diversity_uniqueness` (TextLexicalDiversityUniqueness) - returns the rate between the number of distinct words and the total number of words.

# Installation

To install **kpi** from source you need to download the file `kpi-0.0.1.tar.gz` and run the following command: <!--which is in the folder `dist`. Once this file is installed you can carry on with the instalation using PyPI:-->

```
kpi_library-0.0.2-py3-none-any.whl
```

With this command, any of package dependencies will be installed and you could use the library in your own environment or in local.


# Usage

The steps to use any of the metrics of the library are the following:
1. Create a pandas DataFrame or Series with the data to be analyzed.
2. Initialize the metric that you want to run.
3. Give as parameter the data to process and the rest of parameters in the methods `run` or `to_dqv`, depending on how you want the result.

In the following code there are some examples of how you can use the library.
```
>>> import pandas as pd
>>> import kpi_library
>>>
>>> data = pd.DataFrame([{"Num":0.1209,"Cat":"Female","Date":None,"Text":"He\'s uncertein about his future."},{"Num":0.0665,"Cat":"Male","Date":"02/02/2000","Text":"I want to be a pilot in the future."},{"Num":0.8651,"Cat":"Male","Date":"03/02/2000","Text":"She felt insecure about her future at NASA."},{"Num":0.2336,"Cat":"Female","Date":"04/02/2000","Text":"I can\'t help thinking of the future."},{"Num":0.2336,"Cat":"Female","Date":"05/02/2000","Text":"I can\'t help thinking of the future."}])
>>> data
      Num    Cat2        Date                                         Text
0  0.1209  Female        None             He's uncertein about his future.
1  0.0665    Male  02/02/2000          I want to be a pilot in the future.
2  0.8651    Male  03/02/2000  She felt insecure about her future at NASA.
3  0.2336  Female  04/02/2000         I can't help thinking of the future.
4  0.2336  Female  05/02/2000         I can't help thinking of the future.
>>>
>>> gc = kpi_library.GeneralCompleteness()
>>> gc.run(data)
[{'column_name': 'Num', 'value': 0}, {'column_name': 'Cat', 'value': 0}, {'column_name': 'Date', 'value': 1}, {'column_name': 'Text', 'value': 0}]
>>> gc.to_dqv(data)
[{'dqv_isMeasurementOf': 'general.completeness', 'dqv_computedOn': 'Num', 'rdf_datatype': 'Integer', 'ddqv_hasParameters': [], 'dqv_value': '0'}, {'dqv_isMeasurementOf': 'general.completeness', 'dqv_computedOn': 'Cat', 'rdf_datatype': 'Integer', 'ddqv_hasParameters': [], 'dqv_value': '0'}, {'dqv_isMeasurementOf': 'general.completeness', 'dqv_computedOn': 'Date', 'rdf_datatype': 'Integer', 'ddqv_hasParameters': [], 'dqv_value': '1'}, {'dqv_isMeasurementOf': 'general.completeness', 'dqv_computedOn': 'Text', 'rdf_datatype': 'Integer', 'ddqv_hasParameters': [], 'dqv_value': '0'}]
>>>
>>>
>>> nh = getattr(kpi_library, 'NumericHistogram')()
>>> nh.run(data['Num'], num_bins=3)
[{'limits': '[0.0665, 0.3327)', 'frequency': 4}, {'limits': '[0.3327, 0.5989)', 'frequency': 0}, {'limits': '[0.5989, 0.8651]', 'frequency': 1}]
>>> nh.to_dqv(date['Num'], num_bins=3)
[{'dqv_isMeasurementOf': 'numeric.histogram', 'dqv_computedOn': 'Num', 'rdf_datatype': 'List<Map<String,String>>', 'ddqv_hasParameters': [{'parameter_name': 'num_bins', 'value': '3'}], 'dqv_value': '[{"limits": "[0.0665, 0.3327)", "frequency": 4}, {"limits": "[0.3327, 0.5989)", "frequency": 0}, {"limits": "[0.5989, 0.8651]", "frequency": 1}]'}]
>>>
>>>
>>> cfd = kpi_library.CategoricalFrequencyDistribution()
>>> cfd.run(data['Cat'])
[{'item': 'Female', 'frequency': 3}, {'item': 'Male', 'frequency': 2}]'}]
>>> cfd.to_dqv(data['Cat'], num_items=1)
[{'dqv_isMeasurementOf': 'categorical.frequency_distribution', 'dqv_computedOn': 'Cat', 'rdf_datatype': 'List<Map<String,String>>', 'ddqv_hasParameters': [{'parameter_name': 'num_items', 'value': '1'}], 'dqv_value': '[{"item": "Female", "frequency": 3}, {"item": "OTHER", "frequency": 2}]'}]
>>>
>>>
>>> dm = getattr(kpi_library, 'DateMode')()
>>> dm.run(data['Date'], date_format="%d/%m/%Y")
'2000-02-02 00:00:00'
>>> dm.to_dqv(data['Date'], date_format="%d/%m/%Y")
[{'dqv_isMeasurementOf': 'date.mode', 'dqv_computedOn': 'Date', 'rdf_datatype': 'DateTime', 'ddqv_hasParameters': [{'parameter_name': 'date_format', 'value': '"%d/%m/%Y"'}], 'dqv_value': '2000-02-02 00:00:00'}]
>>>
>>>
>>> tcw = kpi_library.TextCountWords()
>>> tcw.run(data['Text'])
[{'item': 6, 'frequency': 1}, {'item': 9, 'frequency': 1}, {'item': 8, 'frequency': 3}]
>>> tcw.to_dqv(data['Text'])
[{'dqv_isMeasurementOf': 'text.count_words', 'dqv_computedOn': 'Text', 'rdf_datatype': 'List<Map<String,String>>', 'ddqv_hasParameters': [], 'dqv_value': '[{"item": 6, "frequency": 1}, {"item": 9, "frequency": 1}, {"item": 8, "frequency": 3}]'}]
>>>
>>>
>>> bc = getattr(kpi_library, 'BiAnalysisConditionalFrequencyDistribution')()
>>> bc.run(data[['Cat', 'Text']], feature_one='Cat', feature_two='Text', num_items=2)
[{'x_axis': 'Female', 'y_axis': [{'item': 'future', 'frequency': 3}, {'item': 'help', 'frequency': 2}]}, {'x_axis': 'Male', 'y_axis': [{'item': 'future', 'frequency': 2}, {'item': 'want', 'frequency': 1}]}]
>>> bc.to_dqv(data[['Cat', 'Text']], feature_one='Cat', feature_two='Text', num_items=2)
[{'dqv_isMeasurementOf': 'biAnalysis.conditional_frequency_distribution', 'dqv_computedOn': 'Cat, Text', 'rdf_datatype': 'List<Map<String,Serializable>>', 'ddqv_hasParameters': [{'parameter_name': 'num_items', 'value': '2'}, {'parameter_name': 'language', 'value': '"english"'}], 'dqv_value': '[{"x_axis": "Female", "y_axis": [{"item": "future", "frequency": 3}, {"item": "help", "frequency": 2}]}, {"x_axis": "Male", "y_axis": [{"item": "future", "frequency": 2}, {"item": "want", "frequency": 1}]}]'}]
```

## Roadmap
- [X] Develop a database that contains all information related to the implemented metrics.
- [ ] Develop profiling methods for semi- and unstructured data, such as JSON messages, images, or videos.
